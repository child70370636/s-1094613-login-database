<%-- 
    Document   : SelectSample2
    Created on : Sep 30, 2018, 8:46:40 PM
    Author     : lendle
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <a href="AddLogin.jsp">ADD</a><br/>
        <table border="1">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>PASSWORD</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <%
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/test", "root", "");
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery("SELECT * From Login");
                    while(rs.next()){
                %>
                <!--
                加入產生 tr 的程式碼
                -->
                <tr>
                    <td><%=rs.getString("id")%></td>> 
                    <td><%=rs.getString("password")%></td>> 
                    <td><a href="DeleteLogin.jsp?id=<%=rs.getString("id")%>">Delete</a>></td>>
                </tr>
                <%
                    }
                    conn.close();
                %>
            </tbody>
        </table>
    </body>
</html>
